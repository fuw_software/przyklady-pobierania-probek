# gdy nie ma możliwości używać pythona wbudowanego w svarog-lab możliwe jest używanie streamowania do standardu LSL

# włączenie streamowania do LSL:
# w terminalu:
# svarog_streamer -l 
# wypisze listę wzmacniaczy
# szukamy ID odpowiedniego wzmacniacza np:
# * Perun-8 Headset
#       id: "Perun8 1"

# odpalamy stream LSL danego wzmacniacza:
# svarog_streamer -a "Perun8 1" -n "nazwa_streamu"
# nazwa streamu jest ważna, ponieważ streamy są widoczne w sieci LAN
# zaleta streamu jest też taka, że można podglądać go w Svarogu jednocześnie z naszym skryptem python
# po odpaleniu streamu można odpalać poniższy skrypt dowolnym pythonem z zainstalowanym numpy oraz pylsl

from pylsl import StreamInlet, resolve_stream
import time

nazwa_streamu = "nazwa_streamu" # należy odpowiednio zmienić na nazwę użytą w svarog_streamer -n

# znajdujemy streamy
print("szukamy streamy LSL")
streams = resolve_stream('type', 'EEG')

selected_stream = None
# wybieramy nasz
for stream in streams:
	if stream.name() in nazwa_streamu:
		selected_stream = stream
if selected_stream is None:
	print("Nie znalesiono streamu", nazwa_streamu, "w liście", [i.name() for i in streams])
	exit()

# używamy streama
inlet = StreamInlet(selected_stream)

while True:
	# pobieramy próbki (w mikrowoltach)
    sample, timestamp = inlet.pull_chunk(timeout=1.0, max_samples=10)
    print(sample, timestamp, time.monotonic())
