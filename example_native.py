#!/opt/braintech-svarog-lab-python/bin/python3

# powinno się odpalać pythonem z pakietu Svarog-Lab
# lub python w którym są zainstalowane sterowniki z
# https://braintech.pl/pliki/svarog/svarog-streamer-src/svarog-streamer-src-latest.zip
# zaletą jest dostęp wprost do sterownika wzmacniacza
# /opt/braintech-svarog-lab-python/bin/python3


# Alternatywne wzmacniacze
# from braintech.drivers.perun32.amplifier import Perun32Amplifier as Amplifier # duży wzmacniacz na 32 kanały na USB
# from braintech.drivers.perun8.amplifiers import PerunCppAmplifier as Amplifier # headset na 8 kanałów
from braintech.drivers.tmsi.amplifiers import TmsiCppAmplifier as Amplifier # wzmacniacze TMSI

import numpy as np

# Szukamy wzmacniaczy
amps = Amplifier.get_available_amplifiers()
if len(amps) < 1:
    raise Exception("Amplifier not connected")
amp = Amplifier(amps[0])

amp.sampling_rate = 1024 # dla TMSI
# amp.sampling_rate = 500 # dla Perun8, Perun32



def samples_to_microvolts(samples):  # amplifier podaje próbki w integerach, wprost z ADC
    return samples * gains + offsets


amp.start_sampling()
gains = np.array(amp.current_description.channel_gains)
offsets = np.array(amp.current_description.channel_offsets)
while True:
    # pobieramy 16 próbek
    # Proponuje używać IPython dla eksperymentowania
    packet = amp.get_samples(16)
    print(samples_to_microvolts(packet.samples))
    print(packet.ts[0])
    print(packet.samples.shape, amp.current_description.channel_names)

